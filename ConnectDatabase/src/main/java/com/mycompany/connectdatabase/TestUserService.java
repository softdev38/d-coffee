/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.connectdatabase;

import com.mycompany.connectdatabase.model.User;
import com.mycompany.connectdatabase.service.UserService;

/**
 *
 * @author natthawadee
 */
public class TestUserService {
    public static void main(String[] args) {
        UserService userService = new UserService();
        User user = userService.login("user1","password");
        if(user !=null){
            System.out.println("Welcome user : " + user.getName());
            
        }else{
            System.out.println("Error");
        }  
    }
}
