/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.connectdatabase.helper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author natthawadee
 */
public class DatabaseHelper {
   private static Connection conn = null;
       private static String URL = "jdbc:sqlite:dcoffee.db";
       public static  Connection getConnection(){
           if(conn == null){
               try {
                   conn = DriverManager.getConnection(URL);
               } catch (SQLException ex) {
                   Logger.getLogger(DatabaseHelper.class.getName()).log(Level.SEVERE, null, ex);
               }
            System.out.println("Connection to SQLite has been establish.");
           }
           return conn;
       }     
       public static void close() {
        if(conn != null){
            
                try {
                    conn.close();
                    conn = null;
                } catch (SQLException ex) {
                    Logger.getLogger(DatabaseHelper.class.getName()).log(Level.SEVERE, null, ex);
                }
            
        }
        }
       public static int getInsertedId(Statement stmt) {
       try {
           ResultSet key =  stmt.getGeneratedKeys();
           key.next();
           return key.getInt(1);
       } catch (SQLException ex) {
           Logger.getLogger(DatabaseHelper.class.getName()).log(Level.SEVERE, null, ex);
       }
return -1;
    }
    }

